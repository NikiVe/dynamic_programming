<?php


namespace DynamicProgramming;


class FiveToEight
{
    public static function get(int $n) : int {
        $x5 = $x8 = 1;
        $x55 = $x88 = 0;

        for ($k = 2; $k <= $n; $k++) {
            $y55 = $x5;
            $y8 = $x5 + $x55;
            $y5 = $x8 + $x88;
            $y88 = $x8;
            $x5 = $y5;
            $x8 = $y8;
            $x55 = $y55;
            $x88 = $y88;
        }

        return $x5 + $x55 + $x8 + $x88;
    }
}