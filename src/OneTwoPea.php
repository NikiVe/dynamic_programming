<?php
namespace DynamicProgramming;

class OneTwoPea
{
    public static function start(string $line) {
        $numbers = explode('+', $line);
        $one = explode('/', $numbers[0]);
        $two = explode('/', $numbers[1]);

        $a = $one[0];
        $b = $one[1];
        $c = $two[0];
        $d = $two[1];

        $x = $a * $d + $b * $c;
        $y = $b * $d;

        $nod = self::nod($x, $y);
        $x = $x / $nod;
        $y = $y / $nod;

        return $x . '/' . $y;
    }

    public static function even(int $number) : bool {
        return ($number & 1) === 0;
    }

    public static function odd(int $number) : bool {
        return ($number & 1) === 1;
    }

    public static function nod(int $a, int $b) : int {
        if ($a === $b) {
            return $a;
        }
        if (self::even($a) && self::even($b)) {
            return self::nod($a >> 1, $b >> 1) << 1;
        }
        if (self::even($a) && self::odd($b)) {
            return self::nod($a >>1, $b);
        }
        if (self::odd($a) && self::even($b)) {
            return self::nod($a, $b >> 1);
        }
        if ($a > $b) {
            return self::nod(($a - $b) >> 1, $b);
        }
        return self::nod(($b - $a) >> 1, $a);
    }
}