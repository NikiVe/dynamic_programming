<?php


namespace DynamicProgramming;


class Islands
{
    public static function get(array $array) {
        $n = count($array);
        $numOfIslands = 0;
        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $n; $j++) {
                if ($array[$i][$j] === 1) {
                    $numOfIslands ++;
                    $array = self::walk($i, $j, $array, $n);
                }
            }
        }
        return $numOfIslands;
    }

    public static function walk($x, $y, $array, $n) {
        if ($x < 0 || $x >= $n) {
            return $array;
        }

        if ($y < 0 || $y >= $n) {
            return $array;
        }

        if ($array[$x][$y] === 0) {
            return $array;
        }

        $array[$x][$y] = 0;
        $array = self::walk($x - 1, $y, $array, $n);
        $array = self::walk($x + 1, $y, $array, $n);
        $array = self::walk($x, $y - 1, $array, $n);
        $array = self::walk($x, $y + 1, $array, $n);
        return $array;
    }
}