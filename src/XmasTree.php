<?php


namespace DynamicProgramming;


class XmasTree
{
    public static function get(array $array) {
        $n = $array[0][0];

        $tree = [];
        for ($i = 1; $i <= $n; $i++) {
            for ($j = 0; $j <= $i; $j++) {
                $tree[$i][$j] = $array[$i][$j];
            }
        }

        for ($i = $n - 1; $i >= 0; $i--) {
            for ($j = 0; $j <= $i; $j++) {
                $tree[$i][$j] += max($tree[$i + 1][$j], $tree[$i + 1][$j + 1]);
            }
        }
        return $tree[0][0];
    }
}