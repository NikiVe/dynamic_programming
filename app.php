<?php
require_once __DIR__ . '/vendor/autoload.php';

$r = \DynamicProgramming\OneTwoPea::start('1/4+2/8');
var_dump($r);

$tree = [
    [4],
    [1],
    [2,3],
    [4,5,6],
    [9,8,0,3]
];
$r = \DynamicProgramming\XmasTree::get($tree);
var_dump($r);

$r = \DynamicProgramming\FiveToEight::get(88);
var_dump($r);

$ocean = [
    [1, 1, 1, 1, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
    [1, 0, 1, 1, 0],
    [0, 0, 0, 0, 1]
];
$r = \DynamicProgramming\Islands::get($ocean);
var_dump($r);